# -*- coding: utf-8 -*-

######################################################################
#### File_Name: delayCertificates.py                                ##
#### Author: yok                                                    ##
#### Creation_Date: 2020/07/25                                      ##
#### Syntax: Python delayCertificates.py                            ##
######################################################################

######################################################################
###Notice

# Use Python3.0!

######################################################################
###Reference

# https://qiita.com/neet-AI/items/98d4194872ee4f53e3b4
# (1)スクレイピング参考
# https://ameblo.jp/bocchi1219/entry-12408865550.html
# (2)スクショ参考
# https://qiita.com/CyberRex/items/90eb450310f1697d03e9
# (3)Tkinter_GUI画面作成参考
# https://python.keicode.com/advanced/tkinter-widget-combobox.php
# (4)Tkinter_ドロップリスト作成参考
# https://qiita.com/hanzawak/items/2ab4d2a333d6be6ac760
# (5)Chromedriverのversionの合わせ方

######################################################################
###Prerequisite

#>pip install google-api-python-client  
#>pip install PyDrive   # GoogleDriveを使用するために入れる。
#>pip install selenium

######################################################################
###Modules

#from pydrive.drive import GoogleDrive # GoogleDrive用
#from pydrive.auth import GoogleAuth
from selenium import webdriver # スクレイピング用
import sys # 処理を終了させるために使用
import tkinter # GUI画面を作成するために使用
import tkinter.ttk as ttk # ComboBoxを利用するために使用
from tkinter import messagebox # MessageBoxを利用するために使用


######################################################################
###User_Variables
DRIVER_PATH = "\chromedriver_win32\chromedriver.exe"

######################################################################
###variables

#train_list_jr = {"東海道線": "01", "横須賀線・総武快速線": "02", "京浜東北線・根岸線": "03", "横浜線": "14", "南武線": "04", "山手線": "05"}
train_list_metro = {"ginza":"0", "marunouchi":"1", "hibiya":"2", "tozai":"3", "chiyoda":"4", "yurakucho":"5", "hanzomon":"6", "namboku":"7", "fukutoshin":"8"}
metro_dict = {"1":"始発～7時", "5":"7時～8時", "6":"8時～9時", "7":"9時～10時", "3":"10時～17時", "4":"17時～終電"}
metro_h = ["1", "5", "6", "7", "3", "4"]

flg = [False]*4
entry = [None]*3
button = [None]*2
combo = [None]*2
txt = [None]*3
railway = [None]*2
railway[0] = ["JR", "東京メトロ"]
railway[1] = ["ginza", "marunouchi", "hibiya", "tozai", "chiyoda", "yurakucho", "hanzomon", "namboku", "fukutoshin"]
success = "遅延証明書をゲットしました！"
fail = "遅延証明書をゲットできませんでした。"

######################################################################
###Function

# エラーハンドリング関数
def err_handle():
    if txt[2] == "2020":
        messagebox.showinfo("エラー", "遅延した日を入力して下さい。")
        flg[0] = False
    else:
        flg[0] = True

    if combo[0].get() == "":
        messagebox.showinfo("エラー", "遅延した鉄道会社を入力してください。")
        flg[1] = False
    else:
        flg[1] = True

    if combo[0].get() == "JR":
        messagebox.showinfo("ごめんなさい", "JRの処理は現在製作中です。\nメトロを選択して下さい。")
        flg[2] = False
    else:
        flg[2] = True

    if combo[1].get() == "":
        messagebox.showinfo("エラー", "利用した線を選択して下さい。")
        flg[3] = False
    else:
        flg[3] = True


# ウィンドウを閉じる関数
def click_end():
    sys.exit()

# ボタンをクリックしたときに連動する関数
def click_btn():
    for t in range(3):
        txt[t] = entry[t].get()

    train_val = combo[1].get() # 利用路線情報
    txt[2] = "2020" + txt[2] # スクレイピングするために年を入れる。

    err_handle()    # エラー判定
    if False in flg: # falseがあれば、関数を抜ける処理
        return

    for m in metro_h:
        url = "https://www.tokyometro.jp/delay/detail/{}/{}_{}_print.shtml".format(txt[2], train_val, m)
        hours = metro_dict[m]
        file_name = "遅延証明書_{}_{}_{}_{}.png".format(txt[1], txt[0], txt[2], hours)

        # ブラウザを開く処理
        options = webdriver.chrome.options.Options()
        options.add_argument("--headless") # --headlessを付けることでブラウザを表示させない。
        driver = webdriver.Chrome(DRIVER_PATH, chrome_options = options)
        driver.get(url) # これでURLをブラウザで開く
        # ページサイズを設定する処理
        icon = driver.find_elements_by_class_name("v2_headingRouteImg") # 遅延証明書に表示される画像をいれるhtml
        if len(icon)> 0: # iconがあればスクショを取得
            w = driver.execute_script("return document.body.scrollWidth;")  # ページサイズ取得
            h = driver.execute_script("return document.body.scrollHeight;") # ページサイズ取得
            driver.set_window_size(w, h) # スクショサイズ設定
            driver.save_screenshot(file_name) # スクショを取得する処理
            button[0]["text"] = success
            # 取得したスクショの情報をメッセージボックスを表示
            messagebox.showinfo("獲得ファイル情報", "Line : " + train_val + "\n" + hours + "の遅延証明書を獲得しました！")
        else:
            button[0]["text"] = fail

# GUIのベースを表示する関数（メイン関数）
def canvas_up():
    root = tkinter.Tk()
    root.title("遅延証明書Get")
    root.geometry("500x250")

    # 誘導テキストの設置と位置設定
    label1 = tkinter.Label(root, text="名前を教えて下さい （例. 遅延月都男)", font=("Times New Roman", 10))
    label1.place(x=10,y=10)
    label2 = tkinter.Label(root, text="所属を教えて下さい （例. DP2)", font=("Times New Roman", 10))
    label2.place(x=10,y=40)
    label3 = tkinter.Label(root, text="遅延した日を入力して下さい （例. 0417)", font=("Times New Roman", 10))
    label3.place(x=10,y=70)
    label4 = tkinter.Label(root, text="遅延した鉄道会社を選択して下さい", font=("Times New Roman", 10))
    label4.place(x=10,y=100)
    label5 = tkinter.Label(root, text="利用した線を選択して下さい", font=("Times New Roman", 10))
    label5.place(x=10,y=130)

    # テキスト入力ボックスの設置
    for i in range(3):  # もともとは6
        entry[i] = tkinter.Entry(width=20)
        entry[i].place(x=350, y=10+i*30) # テキスト入力ボックスの位置設定

    # ボタンの設置と位置設定
    button[0] = tkinter.Button(text="遅延証明書を取得する。", command = click_btn)
    button[0].place(x=10, y=200)
    button[1] = tkinter.Button(text="画面を閉じる。", command = click_end)
    button[1].place(x=370, y=200)

    # コンボボックスの作成
    for i in range(2):  # もともとは6
        variable = tkinter.StringVar()
        combo[i] = ttk.Combobox(values = railway[i], textvariable = variable)
        combo[i].place(x=350,y=100+i*30) # 位置変更できる。

    root.mainloop() # ウィンドウを表示


######################################################################
###Main

canvas_up()

######################################################################
###Extension

# テキストボックスで質問し、イエスなら保存したものをGoogleドライブにアップロードする。