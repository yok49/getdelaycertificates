# getDelayCertificates

This is a repository for a tool that gets delay certificates.

First of all, check the version of Google Chrome which you use, and get chromedriver whose version is the same for Google Chrome.
After that, put the webdriver into the folder "chromedriver_win32".

Second, install a module, selenium using the command "pip install selenium".
Third, install Tkinter, if you haven't installed it.

Click the bat file, "getDelayCertificates.bat" after you meet the prerequisites.